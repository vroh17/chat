const io = require('socket.io')(4300);

let users = [];

io.on('connection', socket => {
  socket.on('new-user', name => {
    const user = { name, id: socket.id };
    users = [...users, user];
    socket.emit('user-connected', user);
    io.emit('all-users', users);
  });

  socket.on('message', message => {
    io.emit('message', message)
  });

  socket.on('disconnect', () => {
    users = users.filter(user => user.id !== socket.id);
  });
});
