import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { IMessage } from '../message.model';

@Component({
  selector: 'app-chat-feed',
  templateUrl: './chat-feed.component.html',
  styleUrls: ['./chat-feed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatFeedComponent {
  @Input() messages: IMessage[] = [];
}
