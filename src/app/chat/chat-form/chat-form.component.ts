import { Component, Output, EventEmitter, ChangeDetectionStrategy, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ChatService } from '../chat.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatFormComponent implements OnInit, OnDestroy {
  @Output() sendMessageEmitter = new EventEmitter();

  @ViewChild('messageInput', { static: true }) messageInput;

  private alive = true;

  public chatForm: FormGroup = this.formBuilder.group({
    message: ['']
  });
  public emojiCode = '1F60A';

  constructor(
    private formBuilder: FormBuilder,
    private chatService: ChatService
  ) {}

  ngOnInit(): void {
    this.chatService.selectUser$
      .pipe(takeWhile(() => this.alive))
      .subscribe(
      (userName: string) => {
        const user = `@${userName}`;
        this.generateMessage(user);
        this.messageInput.nativeElement.focus();
      }
    );
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  public sendMessage(): void {
    if (this.chatForm.value.message) {
      this.sendMessageEmitter.emit(this.chatForm.value.message);
      this.chatForm.reset();
    }
  }

  public addEmoji(): void {
    this.generateMessage(this.emoji);
    this.messageInput.nativeElement.focus();
  }

  public get emoji(): string {
    return String.fromCodePoint(parseInt(this.emojiCode, 16));
  }

  private generateMessage(value): void {
    const messageControl = this.chatForm.get('message');
    const message = `${messageControl.value || ''} ${value} `;
    messageControl.setValue(message);
  }
}
