import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ChatService } from '../chat.service';
import { IUser } from '../user.model';

@Component({
  selector: 'app-chat-users',
  templateUrl: './chat-users.component.html',
  styleUrls: ['./chat-users.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatUsersComponent {
  @Input() currentUser: IUser;
  @Input() users: IUser[] = [];

  constructor(private chatService: ChatService) {}

  public selectUser(user: string): void {
    this.chatService.selectUser$.next(user);
  }
}
