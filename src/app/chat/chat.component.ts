import { MatDialog } from '@angular/material';
import { takeWhile } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { ChatService } from './chat.service';
import { IMessage } from './message.model';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { IUser } from './user.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  public messages: IMessage[] = [];
  public currentUser: IUser;
  public users: IUser[] = [];

  private alive = true;

  constructor(
    private chatService: ChatService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.openDialog();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  public openDialog(): void {
    const dialogRef  = this.dialog.open(UserDialogComponent, { disableClose: true });
    dialogRef.afterClosed().pipe(takeWhile(() => this.alive)).subscribe((userName: string) => {
      this.chatService.initSocket();
      this.getAllUsers();
      this.chatService.connectUser(userName);
      this.onUserConnect();
      this.onMessage();
    });
  }

  public sendMessage(text: string): void {
    const message = {
      text,
      time: new Date(),
      user: this.currentUser.name
    };
    this.chatService.sendMessage(message);
  }

  private getAllUsers(): void {
    this.chatService.getAllUsers().pipe(takeWhile(() => this.alive)).subscribe(
      users => this.users = users
    );
  }

  private onMessage(): void {
    this.chatService.onMessage().pipe(takeWhile(() => this.alive)).subscribe(
      message => this.messages = [...this.messages, message]
    );
  }

  private onUserConnect(): void {
    this.chatService.onUserConnect().pipe(takeWhile(() => this.alive)).subscribe(
      user => this.currentUser = user
    );
  }
}
