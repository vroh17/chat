import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ChatComponent } from './chat.component';
import { ChatFeedComponent } from './chat-feed/chat-feed.component';
import { ChatFormComponent } from './chat-form/chat-form.component';
import { ChatUsersComponent } from './chat-users/chat-users.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';

@NgModule({
  declarations: [ChatComponent, ChatFormComponent, ChatFeedComponent, ChatUsersComponent, UserDialogComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatListModule
  ],
  entryComponents: [UserDialogComponent],
  exports: [ChatComponent]
})
export class ChatModule { }
