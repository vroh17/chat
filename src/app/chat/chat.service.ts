import * as socketIo from 'socket.io-client';
import { Observable, Subject } from 'rxjs';

import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { IMessage } from './message.model';
import { IUser } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public selectUser$ = new Subject();

  private socket;

  public initSocket(): void {
    this.socket = socketIo(environment.wsUrl);
  }

  public sendMessage(message: IMessage): void {
    this.socket.emit('message', message);
  }

  public onMessage(): Observable<IMessage> {
    return new Observable(observer => {
      this.socket.on('message', data => observer.next(data));
    });
  }

  public getAllUsers(): Observable<IUser[]> {
    return new Observable(observer => {
      this.socket.on('all-users', users => observer.next(users));
    });
  }

  public connectUser(user: string): void {
    this.socket.emit('new-user', user);
  }

  public onUserConnect(): Observable<IUser> {
    return new Observable(observer => {
      this.socket.on('user-connected', user => observer.next(user));
    });
  }
}
