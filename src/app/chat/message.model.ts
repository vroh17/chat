export interface IMessage {
  text: string;
  time: Date;
  user: string;
}
