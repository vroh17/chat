import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDialogComponent {
  public userForm: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required]]
  });

  constructor(private formBuilder: FormBuilder) {}
}
